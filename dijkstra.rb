require_relative 'edge_weighted_digraph'
require_relative 'dijkstra_sp'

class Dijkstra
  def initialize
    @cities         = {}
    @shortest_paths = {}
    @unknown_cities = []
    @usage_text     = <<-END.gsub(/^ {6}/, '')
      Usage: ruby dijkstra.rb GRAPH_DEFINITION_FILE CITY [,CITY …]
    END
  end

  def usage
    puts @usage_text
  end

  def process_args
    unless ARGV.length >= 2
      puts usage
      exit 1
    end
    @graph_file  = ARGV.shift
    @from_cities = ARGV
  end

  def load_graph
    File.open(@graph_file) do |f|
      city_idx = 0
      while (city = f.gets.strip.chomp) != '' do
        @cities[city] = city_idx unless city == ''
        city_idx      += 1
      end

      @ewg = EdgeWeightedDigraph.new @cities.length
      while (edge = f.gets) do
        proc { |from, to, cost|
          @ewg.add_edge DirectedEdge.new(from: @cities[from], to: @cities[to], weight: cost.to_f)
          @ewg.add_edge DirectedEdge.new(from: @cities[to], to: @cities[from], weight: cost.to_f)
        }.call edge.split(',')
      end

    end
  end

  def compute_paths
    @from_cities.each do |city|
      begin
        raise ArgumentError.new("Unknown city #{city}!") if @cities[city].nil?
        @shortest_paths[city] = DijkstraSP.new graph: @ewg, start: @cities[city]
      rescue ArgumentError => e
        puts e
        @unknown_cities << city
        next
      end
    end
  end

  def fix_city_list
    @unknown_cities.each { |unknown_city| @from_cities.delete unknown_city }
  end

  def print_path(edges_ary)
    edges_ary.each_with_object([]) do |stage, path|
      path << @cities.key(stage.to)
    end
  end

  def print_results
    @from_cities.each do |start_city|
      @cities.each_pair do |destination_city, destination_idx|
        next if start_city == destination_city
        print "#{start_city},"
        print print_path(@shortest_paths[start_city].path_to destination_idx).reverse.join(',')
        puts ",#{(@shortest_paths[start_city].dist_to destination_idx).to_i}"
      end
      puts ""
    end
  end

  def run
    process_args
    load_graph
    compute_paths
    fix_city_list
    print_results
  end
end


Dijkstra.new.run