# Dijkstra’s algorithm #

### What is this repository for? ###

This is Ruby implementation of _shortest_paths_ as given in [cos226](http://www.cs.princeton.edu/courses/archive/fall15/cos226/lectures.php).

### How do I run it? ###
`ruby dijkstra.rb graph_definition_file start1 [start2 …]`

### Graph definition forma ###
* List of destinations separated by empty line from list of routes.
* Each route is triple of **start**, **destination** and **distance**.
---
    Paris
    New York
    Dolní Lhota
    Petrůvka
    Horní Lideč
   
    Paris,New York,7
    Paris,Dolní Lhota,1
    Paris,Petrůvka,2
    Paris,Horní Lideč,4
    Dolní Lhota,Petrůvka,1
    Petrůvka,Horní Lideč,1
    Horní Lideč,New York,1

### Output ###
List of shortest paths between each start and every destination with lengths.

`ruby dijkstra.rb <graph> Paris Horní\ Lideč`

    Paris,Petrůvka,Horní Lideč,New York,4
    Paris,Dolní Lhota,1
    Paris,Petrůvka,2
    Paris,Petrůvka,Horní Lideč,3

    Horní Lideč,Petrůvka,Paris,3
    Horní Lideč,New York,1
    Horní Lideč,Petrůvka,Dolní Lhota,2
    Horní Lideč,Petrůvka,1