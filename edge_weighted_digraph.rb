require_relative 'direct_edge'

class EdgeWeightedDigraph
  attr_reader :V, :E, :adjacency_list

  def initialize(number_of_vertices)
    @V, @E          = number_of_vertices, 0
    @adjacency_list = Array.new(number_of_vertices)
    @adjacency_list.map! { |a| a = [] }
  end

  def add_edge(directed_edge)
    @adjacency_list[directed_edge.from] << directed_edge
    @E += 1
  end

  # Returns array of edges pointing from the vertex
  # @param vertex
  # @return [edges] pointing from the vertex
  def adj(vertex)
    @adjacency_list[vertex]
  end

  # Returns array of all edges in this digraph
  # @return [edges]
  def edges
    @adjacency_list.each_with_object([]) do |vertex_edges, a|
      vertex_edges.each { |edge| a << edge }
    end
  end
end
