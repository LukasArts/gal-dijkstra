require_relative 'index_min_pq'

class DijkstraSP

  def initialize(graph:, start:)
    @edge_to = Array.new(graph.V)
    @dist_to = Array.new(graph.V, Float::INFINITY)
    @pq      = IndexMinPQ.new(graph.V)

    @dist_to[start] = 0.0
    @pq.insert(start, 0.0)
    while !@pq.empty? do
      relax(graph, @pq.del_min)
    end
  end

  def relax(graph, vertex)
    graph.adj(vertex).each do |edge|
      w = edge.to
      if @dist_to[w] > @dist_to[vertex] + edge.weight
        @dist_to[w] = @dist_to[vertex] + edge.weight
        @edge_to[w] = edge
        if @pq.contains w
          @pq.change_key(w, @dist_to[w])
        else
          @pq.insert(w, @dist_to[w])
        end
      end
    end
  end

  def dist_to(vertex)
    @dist_to[vertex]
  end

  def has_path_to?(vertex)
    @dist_to[vertex] < Float::INFINITY
  end

  def path_to(vertex)
    return null unless has_path_to? vertex
    path = []
    e    = @edge_to[vertex]
    while !e.nil? do
      path.push e
      e = @edge_to[e.from]
    end
    path
  end

end