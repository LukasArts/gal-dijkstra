class DirectedEdge
  attr_reader :from, :to, :weight

  def initialize( from:, to:, weight: )
    @from, @to, @weight = from, to, weight
  end

  def to_s
    "#{@from} → #{@to} " '%.2f' % @weight
  end
end