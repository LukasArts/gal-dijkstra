class IndexMinPQ

  def initialize(maxN)
    @pq   = Array.new(maxN+1)
    @qp   = Array.new(maxN+1, -1)
    @keys = Array.new(maxN+1)
    @maxN = maxN
    @N    = 0
  end

  def empty?
    @N == 0
  end

  def contains(index)
    @qp[index] != -1
  end

  def size
    @N
  end

  def insert(index, key)
    @N          += 1
    @qp[index]   = @N
    @pq[@N]      = index
    @keys[index] = key
    swim @N
  end

  def min_index
    @pq[i]
  end

  def min_key
    @keys[@pq[1]]
  end

  def del_min
    min = @pq[1]
    exch(1, @N)
    @N -= 1
    sink 1
    @qp[min]         = -1
    @keys[@pq[@N+1]] = nil
    @pq[@N+1]        = -1
    min
  end

  def key(index)
    @keys[index]
  end

  def change_key(index, key)
    @keys[index] = key
    swim @qp[index]
    sink @qp[index]
  end

  def change(index, key)
    change_key(index, key)
  end

  def decrease_key(index, key)
    @keys[index] = key
    swim @qp[i]
  end

  def increase_key(index, key)
    @keys[index] = key
    sink @qp[i]
  end

  def delete(index)
    idx = @qp[index]
    exch idx, @N
    @N -= 1
    swim idx
    sink idx
    @keys[index] = nil
    @qp[index]   = -1
  end

  def greater(i, j)
    @keys[@pq[i]] > @keys[@pq[j]]
  end

  def exch(i, j)
    @pq[i], @pq[j] = @pq[j], @pq[i]
    @qp[@pq[i]]    = i
    @qp[@pq[j]]    = j
  end

  def swim(k)
    while k > 1 && greater(k/2, k)
      exch k, k/2
      k = k/2
    end
  end

  def sink k
    while 2*k <= @N
      j = 2*k
      j += 1 if j < @N && greater(j, j+1)
      break unless greater(k, j)
      exch(k, j)
      k = j
    end
  end

end